import csv
import sys
import asyncio
from typing import Tuple, Dict, Optional, List
import logging

from data_loader.station_record import StationRecord

logger = logging.getLogger(__name__)


class DataLoader:
    def __init__(self, filename: str):
        self.filename = filename

        # stores the lowest temperature as we iterate through the records
        self._lowest_temperature: StationRecord = StationRecord(
            "", "", sys.float_info.max, -1
        )

        # all records indexed by the date
        # Dict[ date, List[StationRecord] ]
        self._records: Dict[str, List[StationRecord]] = dict()

        self._known_dates: List[str] = []

    @classmethod
    async def create(cls, filename: str):
        """
        Factory-like constructor. Creates a new DataLoader obj
        """
        loader = DataLoader(filename)
        await loader.parse()
        return loader

    def _get_values_from_row(
        self, row_index: int, row: dict
    ) -> Optional[Tuple[str, str, float]]:
        station_id = row.get("station_id")
        date = row.get("date")
        temperature_str = row.get("temperature_c")

        if not all([station_id, date, temperature_str]):
            logger.warning(
                f"Invalid row in data file '{self.filename}'. Row number: {row_index+1}"
            )
            return None

        try:
            temperature = float(temperature_str)
        except ValueError as e:
            logger.warning(
                f"Invalid temperature in data file '{self.filename}'. Row number: {row_index+1}. Value: {temperature_str}: {e}"
            )
            return None

        return (station_id, date, temperature)

    @staticmethod
    def _update_fluctuation_dict(
        fluctiation_dict: dict, station_id: str, temperature: float
    ):
        # Tuple[total_fluctuation, last_temperature]
        fluctuation_tuple = fluctiation_dict.get(station_id)
        if fluctuation_tuple:
            fluctuation, last_temperature = fluctuation_tuple
            fluctuation += abs(temperature - last_temperature)
            fluctiation_dict[station_id] = (fluctuation, temperature)
        else:
            fluctiation_dict[station_id] = (0, temperature)

    def _parse_blocking(self):
        """
        blocking version of the parse function. Used internally.
        """
        logger.info(f"parsing data file: {self.filename}")

        total_failed = 0
        total_rows = 0

        with open(self.filename, "r") as f:
            reader = csv.DictReader(f)

            for row in reader:
                total_rows += 1
                values = self._get_values_from_row(reader.line_num, row)

                if not values:
                    total_failed += 1
                    continue

                station_id, date, temperature = values

                record = StationRecord(station_id, date, temperature, reader.line_num)

                # update the lowest temperature
                if temperature < self._lowest_temperature.temperature:
                    self._lowest_temperature = record

                self._records.setdefault(date, list()).append(record)

        # get all known dates sorted. this will make querying faster
        self._known_dates: List[str] = sorted(self._records.keys())

        logger.info(
            f"finished parsing data file with {total_rows} rows. Total rows failed: {total_failed}. Check logs"
        )

    async def parse(self):
        """
        Async-ready parse function. Reads the data file and do some pre-processing
        """
        return await asyncio.get_running_loop().run_in_executor(
            None, self._parse_blocking
        )

    async def get_lowest_temperature(self) -> StationRecord:
        """
        Returns the lowest temperature in this data file along with the date and station_id
        This is pre-calculated while we parse the file, but kept as an async function here in case we need to
        perform extra steps in the future
        """
        return self._lowest_temperature

    async def get_station_with_max_fluctuation(self) -> Tuple[str, float]:
        """
        Returns the station with the max fluctuation considering all the records
        """
        return await self.get_max_fluctiation_in_period(None, None)

    async def get_max_fluctiation_in_period(
        self, start_date: str, end_date: str
    ) -> Tuple[str, float]:
        """
        Returns the maximum amount of fluctuation in the period from <start_date> to <end_date>
        """
        try:
            start_date_index = self._known_dates.index(start_date)
        except ValueError:
            start_date_index = 0

        try:
            end_date_index = self._known_dates.index(end_date)
        except ValueError:
            end_date_index = len(self._known_dates) - 1

        logger.info(
            f"Calculating max fluctuation from known dates: {self._known_dates[start_date_index]} to {self._known_dates[end_date_index]}"
        )

        # Dict[station_id, Tuple[fluctuation, last_temperature]]
        fluctuation_per_station: Dict[str, Tuple[float, float]] = {}

        for date in self._known_dates[start_date_index : end_date_index + 1]:
            for record in self._records.get(date, []):
                self._update_fluctuation_dict(
                    fluctuation_per_station, record.station_id, record.temperature
                )

        station_id, (fluctuation, _) = max(
            fluctuation_per_station.items(), key=lambda item: item[1][0]
        )

        return (station_id, fluctuation)
