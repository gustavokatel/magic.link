import pytest
import os
from data_loader import DataLoader


@pytest.mark.asyncio
async def test_init_with_filename():
    filename = "filename_here.csv"
    loader = DataLoader(filename)
    assert loader.filename == filename


@pytest.mark.asyncio
async def test_create_func_raise_no_file():
    filename = "filename_here.csv"

    with pytest.raises(Exception):
        await DataLoader.create(filename)


@pytest.mark.asyncio
async def test_create_func(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert (await loader.get_lowest_temperature()).station_id == "55"
    assert (await loader.get_lowest_temperature()).temperature == pytest.approx(-11.6)

    assert len(loader._records) == 9


@pytest.mark.asyncio
async def test_create_func_fluctuation(data_file_sample_small):
    loader = await DataLoader.create(data_file_sample_small)

    assert (await loader.get_lowest_temperature()).station_id == "55"
    assert (await loader.get_lowest_temperature()).temperature == pytest.approx(-11.6)

    assert len(loader._records) == 4


@pytest.mark.asyncio
async def test_create_func_with_weird_data(data_file_weird_parsing):
    loader = await DataLoader.create(data_file_weird_parsing)

    assert (await loader.get_lowest_temperature()).station_id == "68"
    assert (await loader.get_lowest_temperature()).temperature == pytest.approx(10.5)

    assert len(loader._records) == 2


@pytest.mark.asyncio
async def test_get_station_with_max_fluctuation(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_station_with_max_fluctuation() == (
        "68",
        pytest.approx(56.9),
    )


@pytest.mark.asyncio
async def test_get_max_fluctuation_default_dates(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_max_fluctiation_in_period(None, "") == (
        "68",
        pytest.approx(56.9),
    )


@pytest.mark.asyncio
async def test_get_max_fluctuation_in_period(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_max_fluctiation_in_period("2000.375", "55,2002.375") == (
        "68",
        pytest.approx(56.9),
    )


@pytest.mark.asyncio
async def test_get_max_fluctuation_in_period_limit_start_date(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_max_fluctiation_in_period("2000.542", "") == (
        "68",
        pytest.approx(51.8),
    )


@pytest.mark.asyncio
async def test_get_max_fluctuation_in_period_limit_end_date(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_max_fluctiation_in_period("2000.542", "2002.208") == (
        "68",
        pytest.approx(47.9),
    )


@pytest.mark.asyncio
async def test_get_max_fluctuation_in_period_limit_end_date_vs_all(data_file_sample):
    loader = await DataLoader.create(data_file_sample)

    assert await loader.get_max_fluctiation_in_period(
        "2000.375", "2002.375"
    ) == pytest.approx(await loader.get_max_fluctiation_in_period(None, None))
