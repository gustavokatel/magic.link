from dataclasses import dataclass


@dataclass
class StationRecord:
    station_id: str
    date: str
    temperature: float
    row_index: int
