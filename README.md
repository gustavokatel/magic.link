# Magic.link coding test

## Usage and requirements:

I used python 3.9

```sh
$ # activate virtualenv (optional)
$ pip install -r requirements.dev.txt
$ python src/main.py
```

## Comments

This was a really fun test. After reading the test description I noticed that the `part 2` was pretty similar to `part 3`. Therefore I used this to save some time during the implementation.

Part 1 was simple. While reading the csv and iterating over the lines I could simple go checking the lowest temperature as we read them. This is not very costly and I found acceptable to be there.

Part 2 and 3 I managed to solve by storing all the records in a dictionary indexed by the date. This way I could easily (and quickly) access all the records from a certain date. Since we would need to query all records from a certain time range, I sorted all the known dates in a separate list using python's builtin sorted function (which has complexity in the worst case `O(nlogn)`). This list was really helpful because I can then iterate over only the known dates instead of the every possible dates within the period. To access the records for each day I use the records dict (read complexity is average `O(1)`). With all the records from the period we calculate the maximum fluctuation and it's done.

## Missing pieces

I added test cases for the `DataLoader` class while I working on it which was essential to spot and solve bugs. The main.py file which contains the CLI integration is lacking unit tests. My next step would be add tests for it by mocking the parser (`DataLoader`) to make sure they are being called correctly.
