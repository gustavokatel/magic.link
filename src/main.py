import asyncio
import sys
import logging

from data_loader import DataLoader

logging.basicConfig(level=logging.DEBUG)


async def main(argv) -> int:
    if not argv or len(argv) < 3:
        print("Usage: python main.py <operation> <file name> [start_date] [end_date]")
        print("Operations: part1, part2, part3")
        print(
            "Start date and end date only available for part3. If undefined, all the range will be considered"
        )
        return 1

    operation = argv[1]
    filename = argv[2]

    start_date = argv[3] if len(argv) >= 4 else None
    end_date = argv[4] if len(argv) >= 5 else None

    loader = await DataLoader.create(filename)

    if operation == "part1":
        record = await loader.get_lowest_temperature()
        print(
            f"station '{record.station_id}' had the lower temperature of {record.temperature}ºC at {record.date} (row #{record.row_index+1})"
        )
    elif operation == "part2":
        station_id, fluctuation = await loader.get_station_with_max_fluctuation()
        print(
            f"Station {station_id} had the higher fluctuation in the whole period: {fluctuation}"
        )
    elif operation == "part3":
        station_id, fluctuation = await loader.get_max_fluctiation_in_period(
            start_date, end_date
        )
        print(
            f"Station {station_id} had the higher fluctuation in the period: {fluctuation}"
        )

    return 0


if __name__ == "__main__":
    sys.exit(asyncio.run(main(sys.argv)))
