import pytest
from pathlib import Path


@pytest.fixture
def data_file_sample(tmp_path):
    data_file: Path = tmp_path / "data.csv"

    data_file.write_text(
        """station_id,date,temperature_c
68,2000.375,10.500
68,2000.542,5.400
68,2000.958,23.000
68,2001.125,20.400
68,2001.292,13.300
68,2001.375,10.400
68,2001.958,21.800
68,2002.208,15.500
68,2002.375,11.600
55,2002.375,-11.600
"""
    )

    return data_file


@pytest.fixture
def data_file_sample_small(tmp_path):
    data_file: Path = tmp_path / "data.csv"

    data_file.write_text(
        """station_id,date,temperature_c
68,2000.375,10.500
68,2002.376,11.600
68,2002.377,-0.600
55,2002.378,-11.600
"""
    )

    return data_file


@pytest.fixture
def data_file_weird_parsing(tmp_path):
    data_file: Path = tmp_path / "data.csv"

    data_file.write_text(
        """station_id,date,temperature_c
68,2000.375,10.500
68,2000.376,10-500
68,2000.377,
68,,10.500
,2000.377,10.500
68,2000.377,10.500
    """
    )

    return data_file
